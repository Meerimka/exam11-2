import React, {Component} from 'react';
import {connect} from "react-redux";
import {fetchCars} from "../../store/actions/carsActions";
import CarListItem from "../../components/CarListItem/CarListItem";
import { Col,  Nav, NavItem, NavLink, Row} from "reactstrap";
import  {NavLink as RouterNavLink } from 'react-router-dom';
import {fetchCategories} from "../../store/actions/categoriesActions";

class Cars extends Component {
    componentDidMount() {
        this.props.onFetchCars();
        this.props.onfetchCategories();
    }



    render() {

        const cars = <div>
            <h2>
                Cars
            </h2>
            {this.props.cars.map(car => (
                <CarListItem
                    key={car._id}
                    _id={car._id}
                    title={car.title}
                    price={car.price}
                    image={car.image}
                />
            ))}

        </div>;
        return (
            <Row  style={{marginTop: "20px"}}>
                <Col sm={3}>
                    <h5>Please select a category: </h5>
                    <Nav vertical>
                        <NavItem>
                            <NavLink tag={RouterNavLink} to="/" exact >All categories</NavLink>
                        </NavItem>
                        {this.props.categories.map(category =>(
                            <NavItem key={category._id}>
                                <NavLink
                                    tag={RouterNavLink}
                                    to={"/categories/" + category._id}
                                    exact
                                >{category.title}
                                </NavLink>
                            </NavItem>
                        ))}
                    </Nav>
                </Col>
                <Col sm={9}>
                    {cars}
                </Col>
            </Row>
        );
    }
}

const mapStateToProps = state => ({
    cars: state.cars.cars,
    categories: state.categories.categories
});

const mapDispatchToProps = dispatch => ({
    onFetchCars: () => dispatch(fetchCars()),
    onfetchCategories: () => dispatch(fetchCategories())
});

export default connect(mapStateToProps, mapDispatchToProps)(Cars);