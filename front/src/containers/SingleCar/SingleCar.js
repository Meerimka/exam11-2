import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {deleteCar, getSingleCar} from "../../store/actions/carsActions";
import CarThumbnail from "../../components/CarThumbnail/CarThumbnail";
import {Button} from "reactstrap";




class SingleCar extends Component {

    componentDidMount(){
        this.props.fetchCar(this.props.match.params.id);
    }

    render() {

        return (
            <Fragment>
                { !this.props.car ? null : <div>
                    <h2 style={{margin: "0 auto"}}>{this.props.car.title}</h2>
                    <p>Price : &nbsp; {this.props.car.price}$</p>
                    <CarThumbnail  image={this.props.car.image}/>
                    <span>{this.props.car.description}</span>&nbsp;
                    <p>Display Name:&nbsp;{this.props.car.user.displayName}</p>
                    <p>Phone number:&nbsp;{this.props.car.user.phone}</p>
                    <p>Car's category:&nbsp; {this.props.car.category.title}</p>
                </div>}
                {this.props.user ? <Button  className="float-left" color="danger" onClick={()=>this.props.delete(this.props.car._id)}>Delete</Button>
                : null}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    car: state.cars.car,
    user:state.users.user
});

const mapDispatchToProps = dispatch =>({
    fetchCar: carId => dispatch(getSingleCar(carId)),
    delete: carId => dispatch(deleteCar(carId))
});

export default connect(mapStateToProps,mapDispatchToProps)(SingleCar);