import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import CarForm from "../../components/CarForm/CarForm";
import {createCar} from "../../store/actions/carsActions";
import {fetchCategories} from "../../store/actions/categoriesActions";


class NewCar extends Component {
  componentDidMount() {
    this.props.fetchCategories();
  }

  createProduct = carData => {
    this.props.onProductCreated(carData).then(() => {
      this.props.history.push('/');
    });
  };

  render() {
    return (
      <Fragment>
        <h2>Create New Car </h2>
        <CarForm
          onSubmit={this.createProduct}
          categories={this.props.categories}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  categories: state.categories.categories
});

const mapDispatchToProps = dispatch => ({
  onProductCreated: carData => dispatch(createCar(carData)),
  fetchCategories: () => dispatch(fetchCategories())
});

export default connect(mapStateToProps, mapDispatchToProps)(NewCar);
