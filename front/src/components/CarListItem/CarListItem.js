import React from 'react';
import PropTypes from 'prop-types';
import {Card, CardBody} from "reactstrap";
import CarThumbnail from "../CarThumbnail/CarThumbnail";
import {Link} from "react-router-dom";

const CarListItem = props => {
  return (
    <Card style={{marginTop: '10px'}}>
      <CardBody>
        <CarThumbnail image={props.image}/>
        <Link to={'/cars/' + props._id}>
          {props.title}
        </Link>
        <strong style={{marginLeft: '10px'}}>
          {props.price} $
        </strong>
      </CardBody>
    </Card>
  );
};

CarListItem.propTypes = {
  image: PropTypes.string,
  _id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired
};

export default CarListItem;
