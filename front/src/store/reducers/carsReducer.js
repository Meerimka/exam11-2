import {FETCH_CAR_SUCCESS, FETCH_CARS_SUCCESS, FETCH_FAIL} from "../actions/carsActions";


const initialState = {
  cars: [],
  car: null,
  error: ''
};

const productsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CARS_SUCCESS:
      return {...state, cars: action.cars};
    case FETCH_CAR_SUCCESS:
      return {...state, car: action.car};
    case FETCH_FAIL:
      return {...state, error: action.error};
    default:
      return state;
  }
};

export default productsReducer;
