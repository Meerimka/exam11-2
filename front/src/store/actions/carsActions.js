import axios from '../../axios-api';
import {push} from "connected-react-router";
import {NotificationManager} from "react-notifications";

export const FETCH_CARS_SUCCESS = 'FETCH_CARS_SUCCESS';
export const FETCH_CAR_SUCCESS = 'FETCH_CAR_SUCCESS';
export const FETCH_FAIL = 'FETCH_FAIL';
export const CREATE_CAR_SUCCESS = 'CREATE_CAR_SUCCESS';

export const fetchCarsSuccess = cars => ({type: FETCH_CARS_SUCCESS, cars});
export const fetchCarSuccess = car => ({type: FETCH_CAR_SUCCESS, car});
export const fetchFail = error => ({type: FETCH_FAIL, error});
export const createCarSuccess = () => ({type: CREATE_CAR_SUCCESS});

export const fetchCars = () => {
  return dispatch => {
    return axios.get('/cars').then(
      response => dispatch(fetchCarsSuccess(response.data))
    );
  };
};

export const createCar = carData => {
  return (dispatch, getState) => {

    const user = getState().users.user;

    if(user === null){
      dispatch(push('/login'));
    }else{
      return axios.post('/cars', carData, {headers: {'Authorization': user.token}}).then(
          () => {
            dispatch(createCarSuccess());
            dispatch(push('/'));
              NotificationManager.success('Added car  successfully ');
          }

      );
    }

  };
};


export const getSingleCar = (carId) => {
  return dispatch => {
    return axios.get('/cars/' + carId).then(
        response => {
          return dispatch(fetchCarSuccess(response.data));

        },

    );
  }
};



export const deleteCar = (carId) => {
    return (dispatch,getState) => {
        const user = getState().users.user;

        if(user === null){
            dispatch(push('/login'));
        }else{
            return axios.delete('/cars/' + carId, {headers: {'Authorization': user.token}}).then(
                () => {
                    dispatch(push('/'));
                    NotificationManager.success('Deleted car  successfully');
                },
                error => dispatch(fetchFail(error))
            );
        }

    }
};


