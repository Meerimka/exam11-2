import axios from '../../axios-api';


export const FETCH_CATEGORIES_SUCCESS = 'FETCH_CATEGORIES_SUCCESS';
export const FETCH_CATEGORY_SUCCESS = 'FETCH_CATEGORY_SUCCESS';

export const fetchCategoriesSuccess = categories => ({type: FETCH_CATEGORIES_SUCCESS, categories});
export const fetchCategorySuccess = category => ({type: FETCH_CATEGORIES_SUCCESS, category});

export const fetchCategories = () => {
  return dispatch => {
    return axios.get('/categories').then(
      response => dispatch(fetchCategoriesSuccess(response.data))
    );
  };
};


export const fetchCategory= (catId) => {
  return dispatch => {
    return axios.get('/categories/' + catId).then(
        response => {
          return dispatch(fetchCategorySuccess(response.data));

        },

    );
  }
};


