import React, {Component, Fragment} from 'react';
import {Container} from "reactstrap";
import {Route, Switch, withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {NotificationContainer} from "react-notifications";

import {logoutUser} from "./store/actions/usersActions";

import Toolbar from "./components/UI/Toolbar/Toolbar";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Cars from "./containers/Cars/Cars";
import NewCar from "./containers/NewCar/NewCar";
import SingleCar from "./containers/SingleCar/SingleCar";
import Categories from "./containers/Categories/Categories";

class App extends Component {
  render() {
    return (
      <Fragment>
          <NotificationContainer/>
        <header>
          <Toolbar user={this.props.user} logout={this.props.logoutUser}/>
        </header>
        <Container style={{marginTop: '20px'}}>
          <Switch>
              <Route path="/" exact component={Cars}/>
              <Route path="/cars/new" exact component={NewCar}/>
              <Route path="/cars/:id" exact component={SingleCar}/>
              <Route path="/categories/:id" exact component={Categories}/>
              <Route path="/register" exact component={Register}/>
            <Route path="/login" exact component={Login}/>
          </Switch>
        </Container>
      </Fragment>
    );
  }
}

const mapStateToProps = state =>({
    user: state.users.user
});

const mapDispatchToProps = dispatch =>({
    logoutUser: () => dispatch(logoutUser())
});

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(App));
