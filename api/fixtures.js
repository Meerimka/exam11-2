const mongoose = require('mongoose');
const config = require('./config');

const Category = require('./models/Category');
const Car = require('./models/Car');
const User = require('./models/User');


const run =async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for(let collection of collections){
        await collection.drop();
    }

    const [user1, user2] = await User.create(
        {username: 'John',
            password: '123',
            displayName: 'John Doe',
            phone: 550445778,
            token: '125425'
        },
        {username: 'Jack',
            password: '123',
            displayName: 'Jack Swim',
            phone: 558996554,
            token: '125425524'
        }
    );

    const [Hatchback,  Sedan, MPV] = await Category.create(
        {title: 'Hatchback', description: 'A hatchback is a car type with a rear door that opens upwards'},
        {title: 'Sedan', description: 'Out of the different types of cars, a sedan (US) or a saloon (UK) is traditionally defined as a car with four doors and a typical boot/ trunk'},
        {title: 'MPV', description: 'A multi-purpose vehicle (MPV) or multi-utility vehicle (MUV) are commonly known as \'people carriers\''},
    );
    const cars = await Car.create(
        {
            title: " Maruti Suzuki Alto 800",
            image: "maruti.jpg",
            user: user1._id,
            description: "It is difficult to imagine a car with more features and a better " +
                "performance at a reasonable Rs 2.99 lakhs than Maruti Alto 800. Not only has " +
                "the engine been refined to keep pace with the present",
            category: Hatchback._id,
            price: 6096
        },
        {
            title: "Hyundai Elantra",
            image: "hyundai.jpg",
            user: user1._id,
            description: "The 6th generation of the Hyundai Elantra has finally" +
                " made its way to India and we drove it to find out what its all about",
            category: Sedan._id,
            price: 28468
        },
        {
            title: "Datsun Go+",
            image: "datsun.jpg",
            user: user2._id,
            description: "If you are looking for a family car that is affordable, " +
                "spacious and fuel-efficient, the Datsun GO+ might fit your requirements.",
            category: MPV._id,
            price: 30000
        }

    );
};

run().catch(error =>{
    console.log('Something wrong happened ...' ,error);
});