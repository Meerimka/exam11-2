const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const auth =require('../middleware/auth');
const Car = require('../models/Car');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', (req, res) => {
    Car.find().populate('category', '_id title').populate('user', 'displayName phone')
        .then(cars => res.send(cars))
        .catch(() => res.sendStatus(500));
});

router.get('/:id', (req, res) => {
    Car.findById(req.params.id).populate('category', '_id title').populate('user', 'displayName phone')
        .then(car => {
            if (car) res.send(car);
            else res.sendStatus(404);
        })
        .catch(() => res.sendStatus(500));
});

router.post('/', [auth, upload.single('image')], async (req, res) => {

    const carData = {
        title: req.body.title,
        description: req.body.description,
        category: req.body.category,
        price: req.body.price,
        user: req.user._id
    };

    if (req.file) {
        carData.image = req.file.filename;
    }

    const car = new Car(carData);

    car.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
});
router.delete('/:id', auth, async (req,res)=>{

    const car = await Car.findOne({
        _id: req.params.id,
        user: req.user._id
    });

    if(!car){
       return res.sendStatus(403);
    }
    await car.remove();
    res.send({message: "OK"})
});



module.exports = router;