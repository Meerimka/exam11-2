const express = require('express');
const Category = require('../models/Category');

const router = express.Router();

router.get('/', (req, res) => {
  Category.find()
    .then(categories => res.send(categories))
    .catch(() => res.sendStatus(500));
});

router.get('/:id', (req, res) => {

  Category.findById(req.params.id)
      .then(car => {
        if (car) res.send(car);
        else res.sendStatus(404);
      })
      .catch(() => res.sendStatus(500));
});

module.exports = router;
