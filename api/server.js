const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');
const users = require('./app/users');
const cars = require('./app/cars');
const categories = require('./app/categories');
const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const port = 8000;

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {
    app.use('/users', users);
    app.use('/cars', cars);
    app.use('/categories', categories);
    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });
});
