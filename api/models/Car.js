const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const  CarSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true,
        min: 0
    },
    description:{
        type: String,
        required: true
    } ,
    image: {
        type: String,
        required: true
    },
    category: {
        type: Schema.Types.ObjectId,
        ref: 'Category',
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
});

const Car = mongoose.model('Car', CarSchema);

module.exports = Car;
